package rahman;
import java.util.Scanner;
public class challange
{
    public static void main(String[] args)
    {
        System.out.println(" ");
        menu();
    }
    public static void menu(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volum");
        System.out.println("------------------------------------");
        System.out.println("Menu :");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volum");
        System.out.println("0. Tutup Aplikasi");
        int menu = input.nextInt();
        if(menu == 1)
        {
            menuLuas();
        }
        else if(menu == 2){
            MenuVolum();
        }
        else if(menu == 0){
            System.out.println(" ");
        }
        else{
            System.out.println("kata kunci salah");
            menuLuas();
        }
    }

    public static void menuLuas(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Pilih Bidang Yang akan dihitung");
        System.out.println("------------------------------------");
        System.out.println("1. persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi panjang");
        System.out.println("0. Kembali Ke Menu Sebelumnya");
        int menu1 = input.nextInt();
        if(menu1 == 1){
            Persegi();
        }
        else if(menu1 == 2){
            Lingkaran();
        }
        else if(menu1 == 3){
            Segitiga();
        }
        else if(menu1 == 4){
            PersegiPanjang();
        }
        else if(menu1 == 0) {
            menu();
        }
    }
    public static void MenuVolum(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Pilih Bidang Yang akan dihitung");
        System.out.println("------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali Ke Menu Sebelumnya");
        int menu2 = input.nextInt();
        if(menu2 == 1)
        {
            Kubus();
        }
        else if(menu2 == 2)
        {
            Balok();
        }
        else if(menu2 == 3)
        {
            Tabung();
        }
        else if(menu2 == 0){
            menuLuas();
        }
    }
    public static void Persegi(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Anda memilih Persegi");
        System.out.println("------------------------------------");
        System.out.print("masukkan sisi ");
        int sisi = input.nextInt();
        System.out.println(" ");
        System.out.println("processing....");
        System.out.println(" ");
        int luas = sisi*2;
        System.out.println("Luas dari persegi adalah"+luas);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        String kembali = input.next();
    }
    public static void PersegiPanjang(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Anda memilih Persegi Panjang");
        System.out.println("------------------------------------");
        System.out.print("masukkan Panjang ");
        int panjang = input.nextInt();
        System.out.print("masukkan Lebar ");
        int lebar = input.nextInt();
        System.out.println(" ");
        System.out.println("processing....");
        System.out.println(" ");
        int luas = panjang * lebar;
        System.out.println("Luas dari persegi Panjang adalah"+luas);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        String kembali = input.next();
    }
    public static void Lingkaran(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Anda memilih Lingkaran");
        System.out.println("------------------------------------");
        System.out.print("masukkan jari-jari ");
        int jari = input.nextInt();
        System.out.println(" ");
        System.out.println("processing....");
        System.out.println(" ");
        double luas = 3.14*jari;
        System.out.println("Luas dari Lingkaran adalah"+luas);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        String kembali = input.next();
    }
    public static void Segitiga(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Anda memilih Segitiga");
        System.out.println("------------------------------------");
        System.out.print("masukkan Alas ");
        int alas = input.nextInt();
        System.out.print("masukkan Tinggi ");
        int tinggi = input.nextInt();
        System.out.println(" ");
        System.out.println("processing....");
        System.out.println(" ");
        int luas = 1/2*alas*tinggi;
        System.out.println("Luas dari segitiga adalah"+luas);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        String kembali = input.next();
    }
    public static void Kubus(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Anda memilih Kubus");
        System.out.println("------------------------------------");
        System.out.print("masukkan sisi ");
        int sisi = input.nextInt();
        System.out.println(" ");
        System.out.println("processing....");
        System.out.println(" ");
        int luas = sisi*3;
        System.out.println("Volum dari Kubus adalah"+luas);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        String kembali = input.next();
    }
    public static void Balok(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Anda memilih Balok");
        System.out.println("------------------------------------");
        System.out.print("masukkan Panjang ");
        int panjang = input.nextInt();
        System.out.print("masukkan Lebar ");
        int lebar = input.nextInt();
        System.out.print("masukkan tinggi ");
        int tinggi = input.nextInt();
        System.out.println(" ");
        System.out.println("processing....");
        System.out.println(" ");
        int luas = panjang * lebar*tinggi;
        System.out.println("Volum dari balok adalah"+luas);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        String kembali = input.next();
    }
    public static void Tabung(){
        Scanner input = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Anda memilih Tabung");
        System.out.println("------------------------------------");
        System.out.print("masukkan jari-jari ");
        int jari = input.nextInt();
        System.out.print("masukkan tinggi ");
        int tinggi = input.nextInt();
        System.out.println(" ");
        System.out.println("processing....");
        System.out.println(" ");
        double luas = 3.14*jari*tinggi;
        System.out.println("Volum dari tabung adalah"+luas);
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        String kembali = input.next();
    }
}
